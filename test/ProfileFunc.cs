﻿using System;
using System.Diagnostics;

namespace CommonTools.Tools
{
    public static class ProfileFunc
    {
        public static void Profile(Action action, string description = "")
        {
            Profile<bool>(() => { action(); return true; }, description);
        }

        public static T Profile<T>(Func<T> action, string description = "")
        {

            StackTrace stackTrace = new StackTrace();
            var name = description.IsNullOrWhiteSpace()?$"{stackTrace.GetFrame(1).GetMethod().DeclaringType.FullName} {stackTrace.GetFrame(1).GetMethod().Name}":description;

#if DEBUG
            Stopwatch watch = Stopwatch.StartNew();
#endif
            try
            {
                //Debug.WriteLine("Start Call {0}", stackTrace.GetFrame(1).GetMethod().Name);
                var ret = action();
#if DEBUG
                watch.Stop();
                Trace.WriteLine($"{name} ellapse {watch.Elapsed}");
#endif
                return ret;
            }
            catch (Exception e)
            {
#if DEBUG
                watch.Stop();
                Trace.WriteLine($"Call action, an exception occure time {name} {watch.Elapsed}");
                Trace.WriteLine($"{e.Message}");

#endif
                return default(T);
            }
        }
    }
}