﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CommonTools.Infrastructure.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
       
        bool PingDatabase();
  
    }
}