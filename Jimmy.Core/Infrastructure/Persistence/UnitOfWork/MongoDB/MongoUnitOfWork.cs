﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jimmy.Core.Infrastructure.Persistence.UnitOfWork.MongoDB;
using MongoDB.Driver;
using CommonTools.Infrastructure.Persistence;
using System.Linq.Expressions;
using MongoDB.Driver.Core.Clusters;
using MongoDB.Bson;

namespace Jimmy.Core.Infrastructure.Persistence.MongoDB.UnitOfWork
{
    public class MongoUnitOfWork : IUnitOfWork
    {
        private IMongoDatabase Database { get; set; }
        private IMongoDatabaseSettings _settings;
        public MongoUnitOfWork(IMongoDatabaseSettings settings)
        {
            try
            {
                _settings = settings;
                var mongoClient = new MongoClient(_settings.ConnectionString);

                Database = mongoClient.GetDatabase(_settings.DatabaseName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public bool PingDatabase()
        {
            bool isConnected = Database.RunCommandAsync(command: (Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (isConnected)
                return Database.Client.Cluster.Description.State == ClusterState.Connected;
            else
                return false;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            //throw new NotImplementedException();
        }
    }
}
