﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jimmy.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Diagnostics;
using System.Security.Claims;
using Jimmy.Infrastructure;

namespace Jimmy.Controllers
{
    public class AccountController : Controller
    {
        private readonly IJwtAuthManager _jwtAuthManager;

        // server side
        public AccountController(IJwtAuthManager jwtAuthManager)
        {
            _jwtAuthManager = jwtAuthManager;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("account/google-login")]
        public IActionResult ExternalLogin()
        {
            //return new ChallengeResult(
            //    GoogleDefaults.AuthenticationScheme,
            //    new AuthenticationProperties
            //    {
            //        RedirectUri = Url.Action(nameof(ExternalLoginCallback), "http://localhost:3000" )
            //    });
            //var redirectUrl = $"https://localhost:44430/account/auth-callback";
            var properties = new AuthenticationProperties { RedirectUri = Url.Action("ExternalLoginCallback") };
            return Challenge(properties, GoogleDefaults.AuthenticationScheme);
        }

        [HttpGet]
        [Route("account/auth-callback")]
        public async Task<IActionResult> ExternalLoginCallback()
        {
            var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var email = result.Principal.FindFirstValue(ClaimTypes.Email);
            if (!result.Succeeded)
                return BadRequest();
            var claims = result.Principal.Identities
                .FirstOrDefault().Claims;
            var Claims = claims.Select(claim => new
            {
                claim.Issuer,
                claim.OriginalIssuer,
                claim.Type,
                claim.Value
            });

            var jwtResult = _jwtAuthManager.GenerateTokens(email, claims, DateTime.Now);
            Object[] myArray = new Object[3];
            myArray[0] = Claims;
            myArray[1] = jwtResult.AccessToken;
            myArray[2] = _jwtAuthManager.DecodeJwtToken(jwtResult.AccessToken);
            return Json(myArray);

        }

    }
}
