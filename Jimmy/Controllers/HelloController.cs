﻿using CommonTools.Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Jimmy.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        private readonly IUnitOfWork _UnitOfWork;

        public HelloController(IUnitOfWork uow)
        {
            _UnitOfWork = uow;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        public IActionResult Get()
        {

            try
            {
                if (_UnitOfWork.PingDatabase())
                    return StatusCode(200, new { Code = "200", Message = "Base de donnée Jimmy accessible." });
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return StatusCode(503, new { Code = "503", Message = "Base de donnée Jimmy NON accessible." });
            }
        }


    }
}
