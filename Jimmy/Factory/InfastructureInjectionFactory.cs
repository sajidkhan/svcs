﻿using CommonTools.Infrastructure.Persistence;
using Jimmy.Core.Infrastructure.Persistence.MongoDB.UnitOfWork;
using Jimmy.Core.Infrastructure.Persistence.UnitOfWork.MongoDB;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
public static class InfrastructureInjectionFactory
{
    public static void InjectionMongoInfrastructure(IServiceCollection services)
    {
        if (!Environment.GetEnvironmentVariables().Contains("CONNECTION_STRING"))
            return;
        services.Configure<MongoDatabaseSettings>(MongoDBParams =>
        {
            MongoDBParams.ConnectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
            MongoDBParams.DatabaseName = Environment.GetEnvironmentVariable("DATABASE_NAME");
        });
        services.AddSingleton<IMongoDatabaseSettings>(sp => sp.GetRequiredService<IOptions<MongoDatabaseSettings>>().Value);
        services.AddScoped<IUnitOfWork, MongoUnitOfWork>();

    }

}
